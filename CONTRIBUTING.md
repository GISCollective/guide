# How to contribute to the documentation

Great! If you are here, it means that you were thinking to improve the Docs.

The main repository where you can contribute to the documentation can be found
here: https://gitlab.com/GISCollective/guide

## Folder structure

This repository contains a folder for each language, and an `img` folder where
pictures used in the docs should be added.

All the documents should be created using the `.md` extension and using the
gitlabs markdown format:

https://docs.gitlab.com/ee/user/markdown.html

## Creating new pages

### Preparation

If there is no issue for creating the new page, on the issue pages:

https://gitlab.com/GISCollective/guide/issues

You can add it one from here:

https://gitlab.com/GISCollective/guide/issues/new

The complete issue documentation can be found here:

https://docs.gitlab.com/ee/user/project/issues/

### Creating the new page

You can create a new branch or merge requestfrom the issue page and open
the Web IDE, for that branch.

Add the link to the page to the table of context, located in the `index.md` corresponding
to the language where you are adding the page. The link to this page should match the
table of contents structure.

For example, if you want to add a page about how a Site can be deleted, you can place it under `manage/sites`. The full path for the new file would be `en/manage/sites/delete.md`.

### Merge request

Once you are happy with your change you can commit the change and ask other for a
short review.

Once everyone involved in the review process, is happy with the new change, it can be
merged with the rest of the documentation.

## Updating existing pages

Every time when you want to update an existing page, you can open the page in
your browser and than you can edit it using the Web IDE.

https://about.gitlab.com/2018/06/15/introducing-gitlab-s-integrated-development-environment/

After you made the change, you should create a Merge Request, and wait for other
contributors to review your changes.

https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html
https://docs.gitlab.com/ee/user/project/merge_requests/


