---
layout: default
title: Overview
nav_order: 1
description: "GISCollective is an open source platform that revolutionizes how we collaborate and participate on maps"
permalink: /
---
<img src="/img/logo.svg" alt="register-form" width="85" align="right"/>

# GeoData for everyone
{: .fs-9 }

GISCollective is an open source platform that revolutionizes how we collaborate and participate on maps.
{: .fs-6 .fw-300 }
---

# Introduction

Welcome to the GISCollective platform. As GeoData gets more accessible, and projects for ecology monitoring, climate change action and social change get more traction, we want to empower organizations and communities that work with maps to make a positive impact.

To find out more about the platform, visit [https://giscollective.com/](https://giscollective.com/).

## How to use this guide

From this guide, learn how to set up your own mapping project using this platform.

- If you are a new mapmaker and don't yet have an account, [start here](/en/Create-account).

- Already have an account? Start by [defining your team](/en/Create-team).

- If your team is set up, skip to [Get started with mapping](/en/Get-started).

If you would like to contribute to the code or install the platform locally, go to [Develop](en/Develop).
{: .fw-500 .text-blue-300}
