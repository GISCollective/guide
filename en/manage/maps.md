# Adding Data: importing and exporting CSVs

This tutorial will show you how to manage the data stored on your GISCollective maps.

GISCollective allows bulk import and export of map data as a CSV!

The CSV (comma-separated values file) is essentially a spreadsheet. Many types of point data can be imported (but files must be under 10MB, multiple uploads are possible).


### Table of Contents

1.  [Importing sites](#import-sites)
2.  [Exporting sites](#exporting-sites)


## Importing sites

GISCollective allows you to import sites to your maps from `CSV` files.

In order to import a file, you have to go to the `Map files` page. The page can be
accessed by going to `Manage -> Maps` and click the 3 dots to open the menu next to the map title:

![the map context menu](../../img/manage/maps/map-context-menu.png "the map context menu")

There, you can upload files, by pressing the upload button:

![the map file upload buton](../../img/manage/maps/map-file-upload-buton.png "the map file upload buton")

Note: In case the file you added does not appear in the list of files after selecting it, please manually refresh the page to see it.

Once your files are uploaded, you can trigger the import action from the file dropdown:

![the map file context menu](../../img/manage/maps/map-file-context-menu.png "the map file context menu")

Note: If your CSV file does not conform to the CSV file format described below, you can also manually map the fields using the `Advanced options`.

The file will be processed in the background and all the valid items will be imported to your map. It may take a few minutes.

### CSV file format

The CSV file must have the following fields, in any order:

#### _id

The site id. If it exists in the database, the existing site will be updated with the csv data. If you are adding new data, leave the cell empty.

#### maps

A list of map ids that the site will be associated with

#### name

The site name

#### description

A text about the site's sustainability and social value. You can use markdown syntax (bold, links, etc) to enrich the format.

#### position.lon

It represents the longitude of the site. The coordinate must be a `double` value using the `WGS84` CRS.

#### position.lat

It represents the latitude of the site. The coordinate must be a `double` value using the `WGS84` CRS.

#### pictures

It contains a list of picture ids associated to the site. The list separator must be the `;` char.

Example:
        ```
        5ca7bfe0ecd8490100cab9d4;5ca7bfd8ecd8490100cab9bf
        ```

#### icons

It contains a list of icon ids associated to the site. The list separator must be the `;` char.
To make the import process more flexible, icon names can also be used in this field instead of ids.

Examples: `5ca7bfe0ecd8490100cab9d4;5ca7bfd8ecd8490100cab9bf`, or `Ecotourism Resource;Green Enterprise`


#### visibility

Set to `0`, `1` or `-1` depending on what you want the visibility of the site to be:

- `0` means you want to import the site as `private`. We recommend using this value so you can review the data before publishing. Bulk actions in the platform enable you to publish all or more sites at once.
- `1` means the site is `public`. It will be visible if the map is public.
- `-1` means the site has state `pending`. The site icon and location will be public on the map, but other data will not be accessible until it is published.

#### attributes

Represents the site's attributes. If they are present, they must be a valid Json object



### Things to know

1. If there is already a site with the same `name` or `id`, it will be replaced with the site from the file
2. If the `maps` field does not exist or the id of the map that you want to import is not set, it will be added by default.
3. You can't set maps that you don't have access to, in the `maps` field. Make sure you can add sites to all maps.
4. It's recommended, if possible, to use ids instead of names
5. It may be easier to add just the primary icon and edit later to add more.

### Sample CSV file data

```
maps,name,description,position.lon,position.lat,pictures,icons,visibility,contributors,attributes
5ca89e37ef1f7e010007f54c,Nice grass field,A nice grass field where you can rest and listen to the birds.,13.4110700117143,52.4757852540638,,5ca7bfececd8490100cab9fa;5ca7bff2ecd8490100caba0c,0,,

```


## Exporting sites

GISCollective allows you to export the sites data of your map in various formats. This enables you, for example, to make bulk edits and import the data again. For users who are logged in, there are 3 options of data formats you can choose from: `CSV`, `GeoJson`, and `GeoPackage`. The export file contains all data, public and private, to which the user has access. For visitors who are not logged in, only 2 options are available, `CSV` and `GeoJson`. The export file will contain only public sites, lines and polygons.

In order to export a map's data, you can use the download button on the Map View page, or the one on the Browse page of the map. Here's how:
* Go to `Browse` in the menu and find your map, then click on the map's name. This will take you to the Map View, where on the lower left corner you have the download button, as shown below.

![download button on map view page](../../img/manage/maps/map-view-download-button.png "download button on map view page")

Note that this flow is also available for site visitors who are not logged in. However, they will only be able to download the public data, so any sites or features that are private will not be included in the exported file.

* Or, if logged in, you can also go to `Browse` and find your map, then click on the 3 dots menu next to the map name and choose `View features`. This will take you to the map's Browse page where you have a download button next to the map name.


![download button on the browse view page](../../img/manage/maps/browse-view-download-button-logged-in.png "download button on the browse view page")

