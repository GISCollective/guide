---
title: Add data to your map
layout: default
nav_order: 5
---


# Add data to your map
{: .fs-9 }
{: .no_toc }

You can add three feature types to your map: __points__, __lines__ and __polygons__. Here is how.
{: .fs-5 .fw-500 }
---

As a mapmaker, once you have a map, an easy way to add data to it is from __+ > Feature__ in the menu.

<img src="/img/add-feature-menu.png" alt="campaign-form" width="200" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

First you enter the name of the feature, then select the map you want to add it to.

In the Geometry section, you set the shape and location. You have more options for doing that:
- you can draw on the map directly,
- specify a geosjon in the embedded editor,
- or upload a file that describes the shape and location(_json_, _geojson_ and _gpx_ file formats are supported).

<img src="/img/geometry-options-1.png" alt="campaign-form" width="200" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

If you choose the first option, drawing on the map directly, you have three further options depending on what you want to add: a point, a line, or a polygon.

<img src="/img/geometry-options-2.png" alt="campaign-form" width="200" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

Here is how to use each option.

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>


## Add points

Points can be used for adding sites, events or anything that can be marked as a single point on your map. To add a point, simply move the center of the map to the desired location. You can zoom in and out as needed to refine the location.

<video width="100%" preload loop autoplay style="
      background-color: #f9f9fb;
      max-width: 100%;
      box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;">
   <source src="/img/add-point.mp4" />
</video>

## Add lines

Lines can be used to map routes on your map, like bike paths, walking trails, etc. To add a line, first select the _create line_ option.

<img src="/img/geometry-line.png" alt="campaign-form" width="200" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

Next navigate on the map to where you want to start drawing your line. Zoom in to refine the location. Place the blue dot following your cursor to the starting point of the line and tap or click once. Then move the cursor to the next point of the line and tap/click again. Repeat this workflow until you have reached the end point of your line. To finish the line, simply tap/click once on the end point.

<video width="100%" preload loop autoplay style="
      background-color: #f9f9fb;
      max-width: 100%;
      box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;">
   <source src="/img/add-line.mp4" />
</video>


If you want it to follow a contour more precisely, now is the time to make adjustments to it. Tap/click on the line to select it (if it's not already selected). As you move your cursor along the line, a blue dot will appear. Simply press and drag that blue dot where you want to refine the line shape.

<video width="100%" preload loop autoplay style="
      background-color: #f9f9fb;
      max-width: 100%;
      box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;">
   <source src="/img/refine-line.mp4" />
</video>

In case you clicked outside the line and deselected it, don't worry. You can put it in edit mode by tapping/clicking on the line.

## Add polygons

Polygons can be used to add areas to your map, like green zones, boundaries, etc. To start, select the _create polygon_ option.

<img src="/img/geometry-polygon.png" alt="campaign-form" width="200" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

Adding a polygon on the map is similar to adding a line, except that to finish drawing a polygon, you tap/click on its first point, so as to close it. Once it’s on the map, users can click anywhere in the area to see the site’s data window. This can sometimes get confusing, so use polygons - especially for boundaries - judiciously.

So let's give it a go: navigate on the map to the location of the starting point of the polygon. Place the cursor there and tap/click to start drawing. Then tap/click again to where the second point of the polygon should be. And so on, until you reach the final point. Then, as mentioned, close the polygon by tapping/clicking on the first polygon point again. And you're done!


<video width="100%" preload loop autoplay style="
      background-color: #f9f9fb;
      max-width: 100%;
      box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;">
   <source src="/img/add-polygon.mp4" />
</video>

You can still make adjustements to the polygon shape by following its contour with your cursor and pressing and dragging the blue dot where refinements are needed.

<video width="100%" preload loop autoplay style="
      background-color: #f9f9fb;
      max-width: 100%;
      box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;">
   <source src="/img/adjust-polygon.mp4" />
</video>

In case you deselected the polygon but want to edit it again, simply click or tap once inside the polygon to reselect it.
