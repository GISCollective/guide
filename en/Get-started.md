---
title: Get started with mapping
layout: default
nav_order: 4
---


# Get started with mapping
{: .no_toc }
{: .fs-9 }

After creating an account and team, you are ready to set up your map! Here is an overview and a few tips to help you get started with confidence.
{: .fs-5 .fw-500 }
---

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

---
## Create your first map

Once a team is in place, it’s time to create a map. The map will contain _features_ that comprise the project data: points (sites), lines (routes) and polygons (areas). Make sure you are logged in and start by pressing on __+ > Map__:

<img src="/img/create-map.png" alt="register-form" width="700" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

When setting up a map, the first step is to name it, provide a description, and associate it to your team. Then you will get to the map’s Edit page, where a wide array of configurations are available.  It’s not required to use all of them, here are the most important ones:

### The Tagline and Cover photos

These are especially important for public projects. Making your map page inviting will encourage participation and clarify what the mapping project is about. So make sure to
- provide a catchy tagline
- upload the two formats for the cover photo - one for narrow screens like a phone, and one for wide screens.

<img src="/img/tagline-cover-photos.png" alt="register-form" width="700" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

### Icon sets

Each feature - whether it be a point, line, or polygon - is visible on the map with its primary icon. The available icons come from the map, so choose an existing icon set for the map, or create one if you want to use your own icons.

Initially, your map will have the default Green Map icons sets selected. You can click on the __pen__ button, then __enable__ to remove some icon sets or add your own.

<img src="/img/icon-sets.png" alt="register-form" width="700" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

Once you selected the desired set or sets, click on Save.
In case you want to create your own icon set, find more info about that on the [About](https://app.giscollective.com/about) page.

### The Map geometry

The geometry defines the "extent" of your map. The extent sets the centerpoint and scale of the map’s initial view. By defining an extent you are creating the default map view for your map. Ideally you want it zoomed in at an appropriate level to show several sites, so your map’s users are encouraged to explore further.

Setting the extent does not draw a border on the map, but centers that area, regardless if viewed on the desktop or a mobile device. The extent is also used to show which maps are available when people propose a site.

To set an extent click on the pen button in the __geometry__ section and:
- Zoom in on the map, and move around to find the right location.
- Once you are over the right map area, position the blue dot where you want to start drawing a rectangle and click there.
- Now drag to make a rectangle of the right shape and size, then click to confirm.

<img src="/img/geometry.png" alt="register-form" width="700" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

Before you save, if you'd like the extent to be a more precise area, press on __Custom Geometry__, then click in the rectangle to select it. You can now place the blue dot anywhere on the rectangle border, and press and drag to adjust the border shape as you wish. Then Save.

<img src="/img/custom-geometry.png" alt="register-form" width="700" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

### The “is public” toggle

As long as it is private, only team members will have access to the map and its data.
You will want to make the map public when it's ready, or if your project includes public participation.

<img src="/img/map_public_toggle.png" alt="register-form" width="700" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

At this point, getting the word out about the map is important to plan. Some projects celebrate with a launch event, post on social media, and [embed their map in their own website](/en/Embed-map)!

## Contribute to the map

Once you have the map, team members and the public at large can start contributing features and making use of the data. GISCollective offers many options for engaging people in the mapmaking process:


- By setting up one or more [__Campaigns__](/en/Set-up-campaigns) for your map, you can effectively crowdsource data gathering, even onsite, while you are in the field. The GISCollective mobile app is perfect for gathering data even if an internet connection is not available on site.
- GISCollective’s menu includes __Propose a site__ so anyone can submit a feature on any map that is public. Their suggested data will be private until you review and publish it.
- Using the import functionality you can add gpx, geojson, json and CSV data already collected.
- As the mapmaker, you can also [add data to your map](/en/Add-map-data) from __+ > Feature__ in the menu. This may be the quickest way to add individual features as a mapmaker, because after filling in the basic information (name, map and location) you go straight to the edit page of the feature, where you can also publish it.
