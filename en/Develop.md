---
title: Develop
layout: default
nav_order: 9
has_children: true
has_toc: false
---

# GISCollective developer manual
{: .fs-9 .no_toc }


This section is for those who want to help with the development process, including source code, software releasing, and other administrative work.
{: .fs-5 .fw-500 }

<details open markdown="block">
  <summary>
    Table of contents:
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

## Setting up the dev environment

Before you start contributing to GISCollective, the following tools need to installed on your system:


### Git

GISCollective source code is stored and version in a git repository on [GitLab](https://gitlab.com/GISCollective). Visit http://git-scm.com/ and https://about.gitlab.com/ for more details.


### DLang compiler

This is the main programming language used for our backend services. For a development environment we prefer to use the [DMD](https://dlang.org/) compiler but [LDC](https://github.com/ldc-developers/ldc) should also work fine. The projects should compile with the latest compiler available.

### Node.js

For the frontend and some backend services we use [Node.js](https://nodejs.org). The latest LTS version should work fine with our projects.

### Mongo

For data persistence, we use the [mongo database](https://www.mongodb.com/). A simple MongoDB Community Server instance should work fine for a development setup: https://www.mongodb.com/try/download/community

### Yarn

For the node projects we use [yarn](https://yarnpkg.com/) with workspaces configurations. Check the installation details at this url: https://yarnpkg.com/getting-started/install

### Ember.js

For the frontend we ues the [ember.js](https://emberjs.com/) the setup guide can be found in this guide: https://guides.emberjs.com/release/getting-started/quick-start/


## Starting the apps

There are multiple components that need to be started in order to have the full setup. You can check the [architecture page](/en/develop/architecture) if you want to better understand the relation between them.

### Minimal setup

To start the web frontend and the api, you need to:

#### 1. Start the frontend project:

  ```
   > git clone git@gitlab.com:GISCollective/frontend/web-frontend.git

   > cd web-frontend

   > yarn

   > yarn start
  ```

#### 2. Start the api server:
  ```
   > git clone git@gitlab.com:GISCollective/backend/ogm-server.git
   
   > cd ogm-server
  ```

  * create an app `config/configuration.js` file based on `config/configuration.model.js`
  * create a db `config/db` folder based on `config/db.model`
  * make sure `mongo` db is running

  ```
   > dub
  ```

When you start the api server for the first time, the database will be seeded with some data. You can log in using the `admin@example.com` email with the `admin` password.

### The complete setup

To have the complete setup you need to run the tasks servers.

#### 3. Start the HMQ server
  
  The HMQ project is the load balancer that we use for the background tasks and it has to be running before the tasks servers are started.

  ```
   > git clone git@gitlab.com:GISCollective/backend/hmq.git
   
   > cd hmq
  ```

  * create an app `config/configuration.js` file based on `config/configuration.model.js`
  * create a db `config/db` folder based on `config/db.model`
  * make sure `mongo` db is running

  ```
   > dub
  ```

#### 4. Start the DLang tasks
  ```
   > git clone git@gitlab.com:GISCollective/backend/tasks.git
   
   > cd tasks
  ```

  * create an app `config/configuration.js` file based on `config/configuration.model.js`
  * create a db `config/db` folder based on `config/db.model`
  * make sure `mongo` db is running

  ```
   > dub
  ```

#### 5. Start the node.js tasks
  ```
   > git clone git@gitlab.com:GISCollective/backend/node.git
   
   > cd node

   > yarn

   > cd packages/tasks
  ```

  * create an app `config/configuration.js` file based on `config/configuration.model.js`
  * create a db `config/db` folder based on `config/db.model`
  * make sure `mongo` db is running

  ```
   > yarn start
  ```

### Other

The node repository also contains the `batch` scripts and the `fonts` server and for starting those projects you need to follow the same steps like the `tasks` server.

  ```
   > git clone git@gitlab.com:GISCollective/backend/node.git
   
   > cd node

   > yarn

   > cd packages/<project>
  ```

  * create an app `config/configuration.js` file based on `config/configuration.model.js`
  * create a db `config/db` folder based on `config/db.model`
  * make sure `mongo` db is running

  ```
   > yarn start
  ```


## Next steps

Once you have the servers running, you can check our [Service Desk](https://gitlab.com/GISCollective/help-desk/-/issues) for the latest issues or questions. Don't forget to read our [Contributing](/en/develop/CONTRIBUTING) and [Code Of Conduct](/en/develop/CODE_OF_CONDUCT) documents. 

For a complete overview of GISCollective you can always check the [infrastructure](/en/develop/architecture) document.

If you have any questions you can contact us by email at [hello@giscollective.com](mailto:hello@giscollective.com) or using the [Service Desk](https://gitlab.com/GISCollective/help-desk/-/issues).