---
title: Create a new account
layout: default
nav_order: 2
---


# Create a new account
{: .fs-9 }

Welcome new Green Mapmaker!

Here are the steps to create an account:

1. Go to [app.giscollective.com](http://app.giscollective.com){:target="_blank"} and click **Sign In** on the upper right.
2. Then click **Register here**.
3. Fill in the registration form as shown below and click **Register**.

You will receive a confirmation email (check your Spam folder if you don't see the email). Click the link in the email to activate your account. Then you are ready to Sign in with your email and password.

<img src="/img/Register-form.png" alt="register-form" width="600" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>
