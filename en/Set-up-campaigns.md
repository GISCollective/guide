---
title: Set up a Campaign
layout: default
nav_order: 2
parent: Campaigns
---


# Set up a Campaign
{: .fs-9 }

Here is how you can create a Campaign in 5 steps.
{: .fs-5 .fw-500 }
---

## Step 1

From the Menu, access the Campaign setup page from __+ -> Campaign__. Write a concise description that explains the motivation of the project, why people should suggest a place for the Green Map, and how their perspective and contribution of knowledge is valued. Adding an attractive image gives the Campaign a more personal touch. Portrait (not landscape) image format is preferred.

<img src="/img/add_campaign_menu.png" alt="campaign-form" width="200" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

## Step 2

Select and save the map to be associated with the Campaign.

<img src="/img/campaign_map.png" alt="campaign-form" width="500" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

## Step 3

Next it’s time to choose the icons. You can set defaulticons that are automatically added to each suggested site. You can choose some optional icons that will appear in the order selected by the submitter. You can add as many icons as you like, but consider your participants and their capacity as you set it up.

For example, if you want to collect data on old trees, you can create and add an Old Tree icon. Select it as default, so that all the sites will have this icon and the attributes that come with it(such as type of tree).

<img src="/img/campaign-add-default-icons.png" alt="campaign-form" width="500" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

As for the optional ones, contributors can choose whether to add any of them or none. For the tree data scenario, these optional icons could be ones that could tell you more about the site, like what context or habitat the tree is located in. You can add as many icons as you like. The optional icons are added from the __questions__ section of the Campaign.

<img src="/img/campaign-add-optional-icons.png" alt="campaign-form" width="500" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

## Step 4

There are some advanced settings you can use to customize the data, questions, and visibility of the Campaign:

If you don’t require a name field for the site contributions, each entry will automatically get an id number. You can set a prefix for that id, so that browsing through data on the map will be easier. For the old trees campaign example, since it is not required that participants give a name to the site, so you could add the prefix “Old tree”. This way all entries will be of the form “Old tree 602f9d163165ac0100aaf000”, “Old tree 323f9d163165ac0100aaf111”, so it’s clear users are looking at a list of old trees entries.

If instead you would like to collect a Name for the site, you can set the first question: “What is the name of this place?”. You also have a second question for the description field. Frame questions to collect the type of information you want about the site.

You also have the option to restrict participation to registered users. Otherwise, anonymous contributors, without being logged in, can also submit answers to Campaigns.

## Step 5

And finally, make your Campaign public, so that people can start participating!

You do that by enabling the _is public_ toggle in the side bar on the right.

<img src="/img/campaign_public_toggle.png" alt="campaign-form" width="500" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

Once the Campaign is public, it will be accessible from Campaigns in the menu, and ready for contributions.
You can make a short URL (e.g. on bit.ly) or QR code for the Campaign. Use the cover image for your social media and other outreach. Let the local news and radio know, stage an event, and otherwise, get the word out!
