---
title: Add your map to your website
layout: default
nav_order: 7
---


# Add your map to your website
{: .fs-9 }

Presenting an interactive map on your website is easy with the option to embed maps. This way your website visitors can explore the map without changing context and the map's latest updates are always reflected.
{: .fs-5 .fw-500 }
---

At the bottom of the Map Edit page, find the embed code in the __Embedded map__ section. It should look similar to this:

<img src="/img/embed-code.png" alt="campaign-form" width="700" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

Copy and paste the whole text on the website you want to embed the map to.
You may need to review how embeds work on your specific CMS or website tool but generally pasting the full HTML text is enough.

Save and see if that worked (you might see a white box, give it a few seconds to load). You can add text that describes the map and links to the full sized, fully featured map on GISCollective.

Also check how the search bar looks: you may want to edit the height of the embedded map, to make sure it is big enough when people interact with the search functionality.

__Note:__ On Squarespace use #3 "Add an Embed Block" section: Alternatively, click the </> icon in the URL field to add or edit the embed code manually. So click that "</>" icon and paste the whole code from the "Embedded map" section. Save and the map should now be embedded.
