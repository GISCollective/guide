---
title: Rest API
layout: default
nav_order: 2
parent: Resources
---


# Our Rest API docs
{: .fs-9 }

Here is the generated OpenApi docs that you can use to access the data.
{: .fs-5 .fw-500 }
---


{% swagger api.json %}