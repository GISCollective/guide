---
title: Publish submitted answers
layout: default
nav_order: 3
parent: Campaigns
---


# Publish submitted answers
{: .fs-9 }

By default, all answers submitted through your Campaign are either pending or private, depending on you Map's settings. Here is how to publish the suggested sites.
{: .fs-5 .fw-500 }
---



## Check the email notification

For every answer submitted through the Campaign, you will get an email notification with the name of your map and a link to the site. Click the link in the email and login if necessary, in order to review the submission.

## Review and publish the site

On the site's page, you will see the site has a visibility status on the upper right corner. Click on edit and change to public, then save.

<img src="/img/edit_site.png" alt="edit-site" width="700" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

<img src="/img/edit_visibility.png" alt="edit-site" width="200" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

That’s all you need to do for this suggested site to appear on your GISCollective!

However, on the edit page of the site you can also update and refine the site data. For example, you can:

- Edit the Title or Description to correct or enhance it. Then click on Save.

  <img src="/img/edit_site_description.png" alt="edit-site" width="500" style= " box-shadow: 0px 0px 5px #ccc;
        -moz-box-shadow: 0px 0px 5px #ccc;
        -webkit-box-shadow: 0px 0px 5px #ccc;
        -khtml-box-shadow: 0px 0px 5px #ccc;"/>

- Tip: adding a website link to the description can be done two ways. You can copy-paste the URL directly or highlight the words you want to link and use the toolbar to add a link. Click the link symbol on the right and paste the URL below, then click your keyboard’s return (or enter) key. You’ll see the linked words. Click Save.

  <img src="/img/set_link_1.png" alt="edit-site" width="300" style= " box-shadow: 0px 0px 5px #ccc;
        -moz-box-shadow: 0px 0px 5px #ccc;
        -webkit-box-shadow: 0px 0px 5px #ccc;
        -khtml-box-shadow: 0px 0px 5px #ccc;"/>

  <img src="/img/set_link_2.png" alt="edit-site" width="300" style= " box-shadow: 0px 0px 5px #ccc;
        -moz-box-shadow: 0px 0px 5px #ccc;
        -webkit-box-shadow: 0px 0px 5px #ccc;
        -khtml-box-shadow: 0px 0px 5px #ccc;"/>

  <img src="/img/set_link_3.png" alt="edit-site" width="300" style= " box-shadow: 0px 0px 5px #ccc;
        -moz-box-shadow: 0px 0px 5px #ccc;
        -webkit-box-shadow: 0px 0px 5px #ccc;
        -khtml-box-shadow: 0px 0px 5px #ccc;"/>

- Change the Primary Icon by clicking the one that you think is most appropriate to appear on the map. You can add more icons, or remove some, too.

  <img src="/img/edit_icons_1.png" alt="edit-site" width="300" style= " box-shadow: 0px 0px 5px #ccc;
        -moz-box-shadow: 0px 0px 5px #ccc;
        -webkit-box-shadow: 0px 0px 5px #ccc;
        -khtml-box-shadow: 0px 0px 5px #ccc;"/>

  <img src="/img/edit_icons_2.png" alt="edit-site" width="300" style= " box-shadow: 0px 0px 5px #ccc;
        -moz-box-shadow: 0px 0px 5px #ccc;
        -webkit-box-shadow: 0px 0px 5px #ccc;
        -khtml-box-shadow: 0px 0px 5px #ccc;"/>

- Have the site appear on multiple maps by clicking on edit for Maps and choosing the additional map(s) where you’d like this site to appear. Click Save.

  <img src="/img/edit_site_maps.png" alt="edit-site" width="500" style= " box-shadow: 0px 0px 5px #ccc;
        -moz-box-shadow: 0px 0px 5px #ccc;
        -webkit-box-shadow: 0px 0px 5px #ccc;
        -khtml-box-shadow: 0px 0px 5px #ccc;"/>

  Change your mind? Click the trash can to remove.

- Adjust the location by clicking on the pen button to the left of Geometry. Then move the pin on the map at the desired location and Save.

  <img src="/img/edit_geometry.png" alt="edit-site" width="500" style= " box-shadow: 0px 0px 5px #ccc;
        -moz-box-shadow: 0px 0px 5px #ccc;
        -webkit-box-shadow: 0px 0px 5px #ccc;
        -khtml-box-shadow: 0px 0px 5px #ccc;"/>
